using System;
using Xunit;
using Xunit.Abstractions;
using XUnitTestingTestverktygLektion7;

namespace XUnitTestingTestverktygLektion7_Test
{
    public class CalculatorFixture : IDisposable
    {
        public Calculator Calculator => new Calculator();

        public void Dispose()
        {
            //Dispose of something code.
        }
    }

    [TestCaseOrderer("XUnitTestingTestverktygLektion7_Test.Orderers.AlphabeticOrderer",
        "XUnitTestingTestverktygLektion7_Test")]
    public class CalculatorTest : IClassFixture<CalculatorFixture> 
    {
        private CalculatorFixture calculatorFixture;

        private ITestOutputHelper output;

        public CalculatorTest(CalculatorFixture calculatorFixtureParameter,
            ITestOutputHelper testOutputHelperParameter)
        {
            calculatorFixture = calculatorFixtureParameter;
            output = testOutputHelperParameter;
        }

        [Theory]
        [InlineData(10, 5, 15)]
        [InlineData(20, 5, 25)]
        [InlineData(14, 5, 19)]
        [InlineData(10, 25, 35)]
        [InlineData(11, -5, 6)]
        [InlineData(10, -10, 0)]
        [InlineData(20, 25, 45)]
        [InlineData(-30, -5, -35)]
        [InlineData(16, 17, 33)]
        [Trait("Calculator", "Add")]
        public void Add_TwoNumbers_GetSum(int value1, int value2, int expected)
        {
            Calculator calculator = calculatorFixture.Calculator;

            output.WriteLine(value1 + " + " + value2 + " = " + expected + "?");

            int actual = calculator.Add(value1, value2);

            Assert.Equal(expected, actual);
        }

        [Fact]
        [Trait("Calculator", "Add")]
        public void Add_10and5_Get15()
        {

            /*Arrange - Given*/
            Calculator calculator = calculatorFixture.Calculator;
            int value1 = 5;
            int value2 = 10;
            int expected = 15;

            /*Act - When*/
            int actual = calculator.Add(value1, value2);

            output.WriteLine("Actual is " + actual);

            /*Assert - Then*/
            Assert.Equal(expected, actual);
        }

        [Fact]
        [Trait("Calculator", "Add")]
        public void Add_4p5and5p2_Get9p7()
        {
            output.WriteLine("This is another message in our test.");

            /*Arrange - Given*/
            Calculator calculator = calculatorFixture.Calculator;
            double value1 = 4.55;
            double value2 = 5.22;
            double expected = 9.8;

            /*Act - When*/
            double actual = calculator.Add(value1, value2);

            /*Assert - Then*/
            Assert.Equal(expected, actual, 1);
        }

        [Fact]
        [Trait("Calculator","Fibonacci")]
        public void FibonacciDoesNotHaveA4()
        {
            Calculator calculator = calculatorFixture.Calculator; ;
            
            Assert.All(calculator.Fibonacci, 
                number => 
                {
                    Assert.NotEqual(0, number);
                    Assert.NotEqual(4, number);
                    
                    });
            
            foreach(int number in calculator.Fibonacci)
            {
                Assert.NotEqual(4, number);
            }
        }

        [Fact]
        [Trait("Calculator", "Fibonacci")]
        public void FibonacciDoesNotHaveA4Version2()
        {
            Calculator calculator = calculatorFixture.Calculator;

            Assert.DoesNotContain(4, calculator.Fibonacci);
        }

        [Fact]
        [Trait("Calculator", "Fibonacci")]
        public void FibonacciDoesHave13()
        {
            Calculator calculator = calculatorFixture.Calculator;

            Assert.Contains(13, calculator.Fibonacci);
        }

        [Fact]
        [Trait("Calculator", "Fibonacci")]
        public void FirstFibonacciBetween0and3()
        {
            Calculator calculator = calculatorFixture.Calculator;
            int actual = calculator.Fibonacci[0];
            Assert.InRange(actual, 0, 3);
        }
    }
}
