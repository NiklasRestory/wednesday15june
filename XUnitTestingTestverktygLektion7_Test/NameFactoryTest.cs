﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using XUnitTestingTestverktygLektion7;

namespace XUnitTestingTestverktygLektion7_Test
{
    [Collection("Name")]
    public class NameFactoryTest
    {
        private NameFixture nameFixture;

        public NameFactoryTest(NameFixture nameFixtureParameter)
        {
            nameFixture = nameFixtureParameter;
        }

        [Fact]
        [Trait("Name", "FullName")]
        public void NameFactory_ShouldGetPersonWhenGivingSocialSecurityNumber()
        {
            Name name = nameFixture.name;

            Name newName = name.NameFactory("324567");
            Assert.IsType<Person>(newName);
        }
    }
}
